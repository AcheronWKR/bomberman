  require("src.board")
  require("src.player")
  require("src.tile")
  require("src.tools")
  require("src.imageAssets")
  
board = {}
player = {}

levelNumber = 1

function love.load()
  if arg and arg[#arg] == "-debug" then require("mobdebug").start() end
  
  loadImages()
  
  loadGame()
  
end

function loadGame()
  --board. initialization
  board = giveMeBoard(levelNumber)
  
  --player. initialization
  player = giveMePlayer()
end

function love.draw()
  love.graphics.setBackgroundColor(255,255,255,255)
   --draw board.
  drawBoard(board)
    
   --draw player
  love.graphics.reset()
  drawPlayer(player)
  
  love.graphics.setColor(255,255,255,255)
  love.graphics.print("FPS: "..love.timer.getFPS(), love.graphics:getWidth()*0.9, love.graphics:getHeight()*0.9)
end

function love.keypressed(key)
   if key == "escape" then
      love.event.quit()
   end
   if key == "1" then
     levelNumber = 1
     loadGame()
   end
   if key == "2" then
     levelNumber = 2
     loadGame()
   end
   if key == "3" then
     levelNumber = 3
     loadGame()
   end
     
end

function love.update(dt)
  -- keyboard. actions for player
  
  -- check for move in vertical and horizontal axis
  local move = {}
  move.v=0
  move.h=0
  
  local moved = false

  if love.keyboard.isDown(" ") then
    putBomb(board, getField(player))
  end


  if love.keyboard.isDown("left") then  
    --movePlayerLeft(player, dt)
    moved = true
    move.h = -1
  elseif love.keyboard.isDown("right") then
    --movePlayerRight(player, dt)
    moved = true
    move.h = 1
  end
  if love.keyboard.isDown("up") then
    --movePlayerUp(player, dt)
    moved = true
    move.v = -1
  elseif love.keyboard.isDown("down") then
    --movePlayerDown(player,dt)
    moved = true
    move.v = 1
  end
  
  if (moved) then
    -- analyze if move is legal due to colysion with board tiles
    local hipoteticalX = giveHipoteticalX(player, dt, move.h)
    local rectH = getRectangle(player)
    rectH.x = hipoteticalX
    if (not checkColisionWithBoard(board, rectH) ) then
      movePlayerHorizontaly(player, dt, move.h)
    end
      
    
    local hipoteticalY = giveHipoteticalY(player, dt, move.v)
    local rectV = getRectangle(player)
    rectV.y = hipoteticalY
    if (not checkColisionWithBoard(board, rectV) ) then
      movePlayerVerticaly(player, dt, move.v)
    end
  end
  
  
end


