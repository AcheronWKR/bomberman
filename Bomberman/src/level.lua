

level1 = {};

level1[0] =  {"X","X","X","X","X","X","X","X","X","X","X","X","X"}
level1[1] =  {"X","Y","Y","Y","Z","Y","Y","Y","Y","Y","Y","Y","X"}
level1[2] =  {"X","Y","X","Y","X","Y","X","Y","X","Y","X","Y","X"}
level1[3] =  {"X","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","X"}
level1[4] =  {"X","Y","X","Y","X","Y","X","Y","X","Y","X","Y","X"}
level1[5] =  {"X","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","X"}
level1[6] =  {"X","Y","X","Y","X","Y","X","Y","X","Y","X","Y","X"}
level1[7] =  {"X","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","X"}
level1[8] =  {"X","Y","X","Y","X","Y","X","Y","X","Y","X","Y","X"}
level1[9] =  {"X","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","X"}
level1[10] = {"X","Y","X","Y","X","Y","X","Y","X","Y","X","Y","X"}
level1[11] = {"X","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","X"}
level1[12] = {"X","X","X","X","X","X","X","X","X","X","X","X","X"}

level2 = {};

level2[0] =  {"X","X","X","X","X","X","X","X","X","X","X","X","X"}
level2[1] =  {"X","Y","Y","Y","Z","Y","Y","Y","Y","Y","Y","Y","X"}
level2[2] =  {"X","Y","X","Y","X","Y","X","Y","X","Y","X","Y","X"}
level2[3] =  {"X","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","X"}
level2[4] =  {"X","Y","X","Y","X","Y","X","Y","X","Y","X","Y","X"}
level2[5] =  {"X","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","X"}
level2[6] =  {"X","Y","X","Y","X","Y","X","Y","X","Y","X","Y","X"}
level2[7] =  {"X","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","X"}
level2[8] =  {"X","Z","X","Y","X","Y","X","Y","X","Y","X","Y","X"}
level2[9] =  {"X","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","X"}
level2[10] = {"X","Y","X","Y","X","Y","X","Y","X","Y","X","Y","X"}
level2[11] = {"X","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","X"}
level2[12] = {"X","X","X","X","X","X","X","X","X","X","X","X","X"}


level3 = {};

level3[0] =  {"X","X","X","X","X","X","X","X","X","X","X","X","X"}
level3[1] =  {"X","Y","Y","Y","Z","Y","Y","Y","Y","Y","Y","Y","X"}
level3[2] =  {"X","Y","X","Z","X","Y","X","Y","X","Y","X","Y","X"}
level3[3] =  {"X","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","X"}
level3[4] =  {"X","Y","X","Z","X","Y","X","Y","X","Y","X","Y","X"}
level3[5] =  {"X","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","X"}
level3[6] =  {"X","Y","X","Y","X","Y","X","Y","X","Y","X","Y","X"}
level3[7] =  {"X","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","X"}
level3[8] =  {"X","Y","X","Y","X","Y","X","Y","X","Y","X","Y","X"}
level3[9] =  {"X","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","X"}
level3[10] = {"X","Y","X","Y","X","Y","X","Y","X","Y","X","Y","X"}
level3[11] = {"X","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","Y","X"}
level3[12] = {"X","X","X","X","X","X","X","X","X","X","X","X","X"}

function getLevel(n)
  if (n==nil) then
    return level1
  end
  
  if (n==1) then
    return level1
  end
    
  if (n==2) then
    return level2
  end
    
  if (n==3) then
    return level3
  end
end 

  