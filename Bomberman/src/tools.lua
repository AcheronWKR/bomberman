function getRectangle(object)
  local rectangle = {}
  rectangle.x = object.x
  rectangle.y = object.y
  rectangle.w = object.w
  rectangle.h = object.h
  
  return rectangle
end

precision = 0.5

function math.round(n, deci) deci = 10^(deci or 0) return math.floor(n*deci+.5)/deci end

function detectHorizontalCollision(o1, o2)
  local r1={}
  r1.left  = o1.x+precision
  r1.right = o1.x + o1.w-precision
  
  local r2={}
  r2.left  = o2.x+precision
  r2.right = o2.x + o2.w - precision
  
  if ( ( r1.left <= r2.left) and (r2.left <= r1.right) ) then
   return true
  end
  
  if ( (r1.left <= r2.right) and (r2.right <= r1.right) ) then
    return true
  end
  
  return false
end

function detectVerticalCollision(o1, o2)
  local r1={}
  r1.top = o1.y + precision
  r1.bot = o1.y + o1.h - precision
  
  local r2={}
  r2.top = o2.y + precision
  r2.bot = o2.y + o2.h - precision

  if ( (r1.top <= r2.top) and (r2.top <= r1.bot) ) then
    return true
  end
    
  if ( (r1.top <= r2.bot) and (r2.bot <= r1.bot) ) then
    return true
  end
  
  return false
end

function detectCollision(o1, o2)
  if (o1 == nil) then
    local nilError = 1
  end
  
  if (o2 == nil) then
    local nilError = 1
  end
  
  if ( detectHorizontalCollision(o1,o2) and detectVerticalCollision(o1,o2) ) then
    return true
  else
    return false
  end
end

    
    
    
    
    