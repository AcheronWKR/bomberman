require("src.level")
require("src.tools")
require("src.bomb")


function giveMeBoard(levelNr)
  local board = {}
  board.Margin=25
  board.x = board.Margin
  board.y = board.Margin
  local screenw = love.graphics:getWidth()
  local screenh = love.graphics:getHeight()
  board.w = screenh - 2 * board.Margin
  board.h = screenh - 2 * board.Margin
  board.sizeX = 13
  board.sizeY = 13
  board.color = {}  
  board.color[0] = 255
  board.color[1] = 255
  board.color[2] = 255
  board.color[3] = 255
  board.bombs = {}
  board.tileSize = board.w/board.sizeX
  board.tiles = makeTilesFromLevel( getLevel(levelNr), board )
  return board
end

function putBomb(board, rectangle)
  if (#board.bombs < player.bomblimit) then
    
    local isClean = true
    for i=1, #board.bombs, 1 do
      if ( (board.bombs[i].x == rectangle.x) and (board.bombs[i].y == rectangle.y)) then
        isClean = false
      end
    end
    
    if(isClean) then
      board.bombs[#board.bombs+1] = Bomb:newBomb(rectangle)
    end
  end
end

function drawBoard(board)
  love.graphics.setColor(board.color[0],board.color[1],board.color[2],board.color[3])
  love.graphics.rectangle("fill", board.x,board.y, board.w, board.h)
  for i=0, #board.tiles, 1 do
    for j=0, #board.tiles[i], 1 do --jest jakas kaszanka tutaj, trzeba zbadac 
      board.tiles[i][j]:drawMe()
    end
  end
  
  for i=1, #board.bombs, 1 do
    board.bombs[i]:drawMe()
  end
  
end

function makeTilesFromLevel(levelTable, board)
  tilesTable = {}
  for i=0, #levelTable, 1 do
    tilesTable[i] = {}
    for j=0, #levelTable[i]-1, 1 do
      tilesTable[i][j] = Tile:newOfKind(levelTable[i][j+1], j, i, board.x, board.y, board.tileSize)
    end
  end
  return tilesTable
end


function checkColisionWithTiles(board, object)
  for i=0, #board.tiles, 1 do
    for j=0, #board.tiles[i], 1 do
      if (board.tiles[i][j]:isBlocking() and detectCollision(board.tiles[i][j], object) ) then
          return true
      end
    end
  end
  return false  
end

function checkColisionWithBombs(board, object)
  local result = false
  for i=1, #board.bombs, 1 do
    if(detectCollision(board.bombs[i], object) ) then
      if(board.bombs[i]:isBlocking()) then
        result = true
      end
    else
      board.bombs[i]:setBlocking()
    end
  end
  return result
end



function checkColisionWithBoard(board, object)
  if (checkColisionWithBombs(board, object) ) then
    return true
  end
  
  if (checkColisionWithTiles (board, object) ) then
    return true
  end
    
end
