require("src.imageAssets")

Tile = {}
Tile.kind = "0"
Tile.x = 0
Tile.y = 0 
Tile.w = 0
Tile.h = 0
Tile.positionX=0
Tile.positionY=0

Tile.blocking = false
Tile.destroyable = false

Tile.color = {}
Tile.color[0] = 0
Tile.color[1] = 0
Tile.color[2] = 0
Tile.color[3] = 255
Tile.texture = getImage("default")


function Tile:new ()
  o = {}   -- create object if user does not provide one
  setmetatable(o, self)
  self.__index = self
  return o
end

function Tile:newOfKind (Kind, X, Y, boardX, boardY, size)
  o = Tile:new()
  o.positionX = X
  o.positionY = Y
  
  o.x = boardX + X*size
  o.y = boardY + Y*size
  
  o.w = size
  o.h = size
  
  o.kind = Kind
  
  if (Kind == "X") then
    o.blocking = true
    o.destroyable = false
    o.texture = getImage( string.lower(o.kind) )
  elseif (Kind =="Y") then
    o.blocking = false
    o.destroyable = false
    o.texture = getImage( string.lower(o.kind) )
  elseif (Kind == "Z" ) then
    o.blocking = true
    o.destroyable = true
    o.texture = getImage( string.lower(o.kind) )
  else
    local x=0
  end
  
  
  
  return o
end

function Tile:isBlocking()
  return self.blocking
end

function Tile:isDestroyable()
  return self.destroyable
end
  

function Tile:drawMe()
  --if(self.kind=="X") then
  --    love.graphics.setColor(0,0,0,255)
  --    love.graphics.rectangle("fill", self.x,self.y, self.w, self.h)
  --elseif(self.kind=="Y") then
  --    love.graphics.setColor(204,204,204,255)
  --    love.graphics.rectangle("fill", self.x,self.y, self.w, self.h)   
  --elseif(self.kind=="Z") then
  --    love.graphics.setColor(255,0,0,255)
  --   love.graphics.rectangle("fill", self.x,self.y, self.w, self.h)
  --end
  if( self.texture) then
    love.graphics.draw(self.texture, self.x,self.y)
  end
end
