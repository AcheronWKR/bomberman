images = {}

function cutOffExtension(filename)
    return string.gsub(filename, "%.%a*", "")
end


function loadImages()
  local files = love.filesystem.getDirectoryItems( "images" )
  for i,v in ipairs(files) do
    local token = cutOffExtension(v)
    images[token] = love.graphics.newImage("images//"..v)
  end
  
  local i=0
end

function getImage(token)
  return images[token]
end