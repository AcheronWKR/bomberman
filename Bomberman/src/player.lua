require("src.imageAssets")
require("src.tools")

function giveMePlayer()
  local player = {}
  player.x=board.x+board.tileSize
  player.y=board.y+board.tileSize
  player.Size=board.w/board.sizeX
  player.w=player.Size
  player.h=player.Size
  player.color1 = {}
  player.color1[0] = 128
  player.color1[1] = 255
  player.color1[2] = 128
  player.color1[3] = 255
  player.color2 = {}
  player.color2[0] = 0
  player.color2[1] = 0
  player.color2[2] = 0
  player.color2[3] = 255
  player.speed = 100
  player.bomblimit = 100
  player.texture = getImage("player")
  return player
end

function drawPlayer(player)
    --love.graphics.setColor(player.color2[0],player.color2[1],player.color2[2],player.color2[3])
    --love.graphics.circle("fill",player.x+player.w/2,player.y+player.h/2,player.Size/2)
    
    --love.graphics.setColor(player.color1[0],player.color1[1],player.color1[2],player.color1[3])
    --love.graphics.circle("fill",player.x+player.w/2,player.y+player.h/2,(player.Size*0.8)/2)
    --love.graphics.rectangle("fill", player.x, player.y, player.w, player.h)
    love.graphics.draw(player.texture, player.x, player.y)
end

function movePlayerLeft(player, dt)
  if(board.x<player.x) then
    player.x = player.x - player.speed*dt
  end
end

function movePlayerRight(player, dt)
  if( (board.x + board.w) > (player.x + player.w) ) then
    player.x = player.x + player.speed*dt
  end
end

function movePlayerUp(player, dt)
  if(board.y<player.y) then
    player.y = player.y - player.speed*dt
  end
end

function movePlayerDown(player, dt)
  if( (board.y + board.h) > (player.y + player.h) ) then
    player.y = player.y + player.speed*dt
  end
end

function giveHipoteticalX(player, dt, h)
  return player.x + player.speed*dt*h
end
  
function giveHipoteticalY(player, dt, v)
  return player.y + player.speed*dt * v
end
  
  
function movePlayerVerticaly(player, dt, v)
  player.y = player.y + player.speed*dt * v
end

function movePlayerHorizontaly(player, dt, h)
  player.x = player.x + player.speed*dt * h
end

function getField(player)
  local rect = {}
  rect.x = board.x + board.tileSize * math.round( (player.x - board.x) / board.tileSize)
  rect.y = board.y + board.tileSize * math.round( (player.y - board.y) / board.tileSize)
  rect.w = board.tileSize
  rect.h = board.tileSize
  return rect
end 
  
  